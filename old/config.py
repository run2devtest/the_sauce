from configparser import ConfigParser
from os import path

BASE_PATH = path.dirname(path.abspath(__file__))
CONFIG_FILE = path.join(BASE_PATH, 'settings.cfg')

config = ConfigParser()


def createDefaultConfigFile():
    with open(CONFIG_FILE, 'w') as configfile:
        pass

createDefaultConfigFile()
