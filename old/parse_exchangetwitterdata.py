from dbmodels import ExchangeData, ExchangeTwitterData
from twitterparser import getUserStats


def main():
    for name, twitter_handle in ExchangeData.get_all_twitter_names().items():
        data = {**{'name': name}, **getUserStats(twitter_handle)}
        ExchangeTwitterData.addData(**data)
        print(data, '\n')


if __name__ == '__main__':
    main()
