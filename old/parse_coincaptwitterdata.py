from datetime import datetime
from dbmodels import CoinCapData, CoinCapTwitterData
from twitterparser import getUserStats


def getCurrentDateTime():
    return datetime.utcnow()


def main():
    time = {'created_date': getCurrentDateTime()}
    for cid, twitter_handle in CoinCapData.get_all_twitter_names().items():
        data = {**{'cid': cid}, **getUserStats(twitter_handle), **time}
        CoinCapTwitterData.addData(**data)
        print(data, '\n')


if __name__ == '__main__':
    main()
