from gettoken import GetToken
from logger import getLogger

from os import listdir, makedirs, path, remove

import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Job
from time import sleep

from datetime import datetime
import controller

BASE_PATH = path.dirname(path.abspath(__file__))
CHART_PATH = path.join(BASE_PATH, 'charts')


def getUsers():
    return ['371043710', '486202514', '502311995', '393533830', '539378582', '382295911']
    # 393533830 moi
    # 382295911 sanchez
    # return ['371043710']


def send_chart(bot, job):
    for user in getUsers():
        for chart in listdir(CHART_PATH):
            bot.send_photo(chat_id=user, photo=open(
                path.join(CHART_PATH, chart), 'rb'))
            print('{} sent to user {}'.format(chart, user))
        sleep(5)

    # for chart in listdir(CHART_PATH):
    #     for user in getUsers():
    #         bot.send_photo(chat_id=user, photo=open(
    #             path.join(CHART_PATH, chart), 'rb'))
    #     sleep(1)


def sendmessage(bot, job):
    message = 'Charts Processed {} UTC TIME'.format(datetime.utcnow())
    message += '+\n'
    # message += 'TESTING!!!' * 5
    for x in getUsers():
        bot.send_message(chat_id=x, text=message,
                         parse_mode=telegram.ParseMode.HTML)


def endmessage(bot, job):
    message = 'Brought to you by FOMO DRIVEN DEVELOPEMENT.\n'
    # message += 'TESTING!!!' * 5
    for x in getUsers():
        bot.send_message(chat_id=x, text=message,
                         parse_mode=telegram.ParseMode.HTML)


def toppicksmessage(bot, job):
    message = 'TOP PICKS: ' + controller.get_number_one()
    for x in getUsers():
        bot.send_message(chat_id=x, text=message,
                         parse_mode=telegram.ParseMode.HTML)


def run_bot(run=False):
    if run:
        updater = Updater(GetToken())
        job = updater.job_queue

        job.run_once(sendmessage, 0)
        job.run_once(send_chart, 0)
        job.run_once(toppicksmessage, 0)
        job.run_once(endmessage, 0)

        updater.start_polling()
        updater.stop()

if __name__ == '__main__':
    run_bot(run=True)
