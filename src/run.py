from crontab import CronTab
from os import path
import getpass

from coinmarketscraper import writeDataToCoinCapdatabase
from exchangescraper import getAllExchangeInfo
from dbmodels import cleanTables
from parsetwitterdata import parseAllTwitterData
from logger import GetLogger

import atexit
from os import path
from json import dumps, loads

_logger = GetLogger()

BASE_PATH = path.dirname(path.abspath(__file__))
COINMARKET_SCARPER = 'python3 ' + path.join(BASE_PATH, 'coinmarketscraper.py')
EXCHANGE_SCAPER = 'python3 ' + path.join(BASE_PATH, 'exchangescraper.py')
TWITTERDATA_PARSER = 'python3 ' + path.join(BASE_PATH, 'parsetwitterdata.py')
BOT = 'python3 ' + path.join(BASE_PATH, 'bot.py')

COUNTER_PATH = path.join(BASE_PATH, 'counter.json')


def read_counter():
    try:
        with open(COUNTER_PATH, 'r') as file:
            return loads(file.read()) + 1
    except FileNotFoundError:
        return 0


def write_counter(counter):
    with open(COUNTER_PATH, 'w') as file:
        file.write(dumps(counter))


def run():
    username = getpass.getuser()
    _logger.info('Username: {}'.format(username))
    taskr = CronTab(user=username)

    job1 = taskr.new(command=COINMARKET_SCARPER)
    job2 = taskr.new(command=EXCHANGE_SCAPER)
    job3 = taskr.new(command=TWITTERDATA_PARSER)
    job4 = taskr.new(command=BOT)
    if read_counter() < 1:
        _logger.info('running the_sauce script for the first time...')
        cleanTables()
        writeDataToCoinCapdatabase()
        getAllExchangeInfo()

    job1.day.every(1)
    job1.hour.on(0)
    job1.minute.on(0)

    job2.day.every(1)
    job2.hour.on(0)
    job2.minute.on(0)

    job3.minute.every(30)

    job4.minute.every(60)
    taskr.write()
    _logger.info('{}written to crontab.'.format(taskr.render()))
    _logger.info('this script has been run {} times.'.format(read_counter()))


if __name__ == '__main__':
    run()
    counter = read_counter()
    atexit.register(write_counter, counter)
