from dbmodels import ExchangeData, CoinCapData


def getBTCPairs(name):
    exchange = ExchangeData.get_row_by_name(name)
    return [x for x in exchange.btc_pairs.split(',')]


def getETHPairs(name):
    exchange = ExchangeData.get_row_by_name(name)
    return [x for x in exchange.eth_pairs.split(',')]


def getPoloniexCoins():
    return getBTCPairs('poloniex')


def getBittrexCoins():
    return getBTCPairs('bittrex')


def getLiquiCoins():
    return getBTCPairs('liqui')


def getBinanceCoins():
    return getBTCPairs('binance')


def getEtherDeltaCoins():
    return getETHPairs('etherdelta')


def getLowVolFilterShitCoins():
    coins = getBTCPairs('cryptopia')
    coins += getBTCPairs('coinsmarkets')
    coins += getBTCPairs('trade-satoshi')
    coins += getBTCPairs('c-cex')
    coins += getBTCPairs('coinexchange')
    coins += getBTCPairs('stocks-exchange')
    coins += getBTCPairs('mercatox')
    coins = list(set(coins))
    coins = returnFilterListOfNames(coins)
    return coins


def getSogFilterCoins():
    coins = getPoloniexCoins()
    coins += getBittrexCoins()
    coins += getLiquiCoins()
    coins = list(set(coins))
    coins = returnFilterListOfNames(coins)
    return coins


def getSogFilterPlusCoins():
    coins = getPoloniexCoins()
    coins += getBittrexCoins()
    coins += getLiquiCoins()
    coins = list(set(coins))
    coins = returnFilterListOfNames(coins)
    return coins


def byPoloniexFilter(element):
    return element in getPoloniexCoins()


def filterListByPoloFilter(coins_list):
    return [x for x in filter(byPoloniexFilter, coins_list)]


def returnFilterListOfNames(coin_list):
    coindict = CoinCapData.get_all_symbols_and_cids()
    newlist = []
    for coins in coin_list:
        try:
            newlist.append(coindict[coins])
        except KeyError:
            # print(coins, 'doesnt exist')
            pass

        # try:
        #     newlist.remove('blocktix')  # Used this area to remove bad data
        # except ValueError:
        #     pass
        # try:
        #     newlist.remove('monacocoin')  # Used this area to remove bad data
        # except ValueError:
        #     pass
    return newlist


def filterCoinsByExhangeVol(exchange, filterExchangeOut=None):
    exchanges = exchange()

    if filterExchangeOut:
        for exch in filterExchangeOut:
            try:
                exchanges.remove(exch)
            except ValueError:
                pass
    exchangelist = []
    exchangenames = []
    for exchange in exchanges:
        exchangenames.append(ExchangeData.get_row_by_name(exchange).name)
        templist = ExchangeData.get_row_by_name(exchange).btc_pairs.split(',')

        if templist:
            exchangelist += templist

    cleaned = list(filter(None, list(set(exchangelist))))
    return returnFilterListOfNames(cleaned)


def getBillionFilterCoins():
    exchange = ExchangeData.get_exchanges_vol_1B
    return filterCoinsByExhangeVol(exchange)


def get500MillionFilterCoins():
    exchange = ExchangeData.get_exchanges_vol_500M
    return filterCoinsByExhangeVol(exchange)


def getLessThan100MillionFilterCoins():
    exchange = ExchangeData.get_exchanges_vol_less_100M
    return filterCoinsByExhangeVol(exchange)


def getLessThan10MillionFilterCoins():
    exchange = ExchangeData.get_exchanges_vol_less_10M
    return filterCoinsByExhangeVol(exchange)


def getBillionFilterCoinsNoBinace():
    exchange = ExchangeData.get_exchanges_vol_1B
    return filterCoinsByExhangeVol(exchange, filterExchangeOut=['binance', 'hitbtc', 'huobi', 'okex'])


def get500MillionFilterCoinsNoBinace():
    exchange = ExchangeData.get_exchanges_vol_500M
    return filterCoinsByExhangeVol(exchange, filterExchangeOut=['binance', 'hitbtc', 'huobi', ])

if __name__ == '__main__':
    getBillionFilterCoinsNoBinace()
    get500MillionFilterCoinsNoBinace()
# # EXHANGE FILTER
# TODO: Dynamic Filter, Exchanges > 1B/100M
# TODO: SOG FILTER - POLO, BITTREX, LIQUI
# TODO: SOG FILTER PLUS - ED, BINANCE
# TODO: SINGLE EXCHANGE - POLO, BITTREX, LIQUI

# COIN FILTERS
# TODO: CIRCULATING SUPPLY FILTER
# TODO: AVERAGE VOL
