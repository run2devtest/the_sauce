from ccxt.base.errors import ExchangeError, RequestTimeout
from time import sleep, time
import sys
from datetime import datetime as dt

from exchangelink import ExchangeLink
from strategies import mfi
from strategies import ichimoku
from subprocess import DEVNULL, STDOUT, check_call
import pandas as pd
import atexit
import config
import bot2 as bot

import copy

SOUND = config.getSoundSetting()
EXCHANGE = 'binance'
PERIODS = ['4H']

color_change_set = set()
mfi_cross_set = set()
mfi_over_set = set()


def clear_alert_set():
    color_change_set.clear()
    mfi_cross_set.clear()
    mfi_over_set.clear()


def playsound1():
    if SOUND:
        check_call(['/usr/bin/aplay', 'sound.wav'],
                   stdout=DEVNULL, stderr=STDOUT)


def playsound2():
    if SOUND:
        check_call(['/usr/bin/aplay', 'sound2.wav'],
                   stdout=DEVNULL, stderr=STDOUT)


def check_colorchange(data, coin, timeframe):
    df = data.tail(2)
    close = data.tail(1)
    close = str(close['close'].values[0])
    colors = list(df['candle_color'])
    if len(set(colors)) > 1:
        if colors[1] == 'green':
            t = dt.now().strftime("%I:%M:%S")
            alert_name = str(coin + timeframe)
            if alert_name not in color_change_set:
                message = ('{} {} \nChanging from {} to {} \nPrice:{} Time: {} \nExchange: {}'.format(
                    coin, timeframe, colors[0].title(), colors[1].title(), close, t, EXCHANGE.upper()))
                print(message)
                bot.sendAlert(message)
                playsound1()
            return alert_name


def check_mfi(data, coin, timeframe):

    df = data.tail(2)
    last = list(df['MFI'][:-1])[0]
    current = list(df['MFI'][1:])[0]
    if last < 50:
        if current > 50:
            t = dt.now().strftime("%I:%M:%S")
            alert_name = str(coin + timeframe)
            if alert_name not in mfi_cross_set:
                message = (
                    '{} {} \nMFI 50% Cross-Up \nTime: {} \nExchange: {}'.format(coin, timeframe, t, EXCHANGE.upper()))
                print(message)
                bot.sendAlert(message)
                playsound2()
            return alert_name


def check_mif_over(data, coin, timeframe):
    df = data.tail(2)
    mfi_over = list(df['MFI_Over_Signal'])

    if len(set(mfi_over)) > 1:
        t = dt.now().strftime("%I:%M:%S")
        alert_name = str(coin + timeframe)
        if alert_name not in mfi_over_set:
            message = (
                '{} {} \nMFI Over Bought/Sold \nTime: {} \nEXCHANGE: {}'.format(coin, timeframe, t, EXCHANGE.upper()))
            print(message)
            bot.sendAlert(message)
            playsound2()
        return alert_name


@atexit.register
def goodbye():
    bot.sendAlert('{} BOT HALTED'.format(EXCHANGE.upper()))


def main(cycle_time):

    exchange = ExchangeLink(EXCHANGE)
    coin_pairs = exchange.get_exchange_coin_pairs()
    coin_length = len(coin_pairs)

    print('Found {} coins on {}'.format(coin_length, EXCHANGE))
    print(coin_pairs)

    cloud_pos = {'Below': [], 'Above': [],
                 'Inside': [], 'Unknown': []}

    cloud_position = dict([(p, copy.deepcopy(cloud_pos)) for p in PERIODS])
    timed_out = []

    for ticker in coin_pairs:
        try:

            print('Processing: {} Cycle Time: {:.2f}'.format(ticker, cycle_time))
            data = exchange.get_historial_data(ticker)

            for period in PERIODS:

                n_data = data[period]

                ichi_data = ichimoku.Ichimoku(n_data)
                n_data = ichi_data.get_ichimoku_data()
                n_data = mfi.MFI(n_data).get_money_flow_data()

                color_change_set.add(check_colorchange(n_data, ticker, period))
                mfi_cross_set.add(check_mfi(n_data, ticker, period))
                mfi_over_set.add(check_mif_over(n_data, ticker, period))

                # ichimoku summary
                position = ichi_data.get_price_cloud_relation()
                cloud_position[period][position].append(ticker)
            sleep(exchange.get_rate_limit())

        except ExchangeError:
            pass

        except RequestTimeout:
            print(ticker, 'TimedOut.')
            timed_out.append(ticker)

        except KeyboardInterrupt:
            print('Exiting...')
            sys.exit()

    for cloud_summary in PERIODS:

        message = str(
            '{} {} Cloud Summary\n'.format(EXCHANGE.upper(), cloud_summary))
        message += str('Above cloud: ' +
                       str(cloud_position[cloud_summary]['Above']) + '\n')
        message += str('Inside cloud:' +
                       str(cloud_position[cloud_summary]['Inside']) + '\n')
        message += str('Below cloud:' +
                       str(cloud_position[cloud_summary]['Below']) + '\n')
        message += str('Processing Error: ' + str(cloud_position[cloud_summary][
            'Unknown']) + '\n')
        message += str('Timedout:' + str(timed_out) + '\n')
        message += str('GREEN/RED CHANGE: {}'.format(color_change_set) + '\n')
        message += str('MFI 50% CROSS: {}'.format(mfi_cross_set) + '\n')
        message += str('MFI OVER B/S: {}'.format(mfi_over_set) + '\n')
        print(message)

if __name__ == '__main__':
    bot.sendAlert('STARTING {} BOT'.format(EXCHANGE.upper()))
    start_time = time()
    end_cycle_time = (time() - start_time) // 60
    while True:
        main(end_cycle_time)
        end_cycle_time = (time() - start_time) // 60
        if end_cycle_time >= config.getCycleTime():
            print('Restarting ALERTS/CYCLETIME')
            clear_alert_set()
            start_time = time()
        print('Cycle Time: {:.2f} Complete'.format(end_cycle_time))
        sleep(config.getCycleSleepTime())
