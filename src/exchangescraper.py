from bs4 import BeautifulSoup
from datetime import datetime
from dbmodels import ExchangeData
from logger import GetLogger
from time import time
from deco import concurrent, synchronized

import requests

_logger = GetLogger()


def convertDollarsToInt(dollars):
    try:
        return int(dollars.replace(',', '').replace('$', ''))
    except TypeError:
        return 0


def convertBTCToFloat(btc):
    try:
        return float(btc.replace(',', '').replace('BTC', '').replace(' ', ''))
    except TypeError:
        return 0


def getCurrentDateTime():
    return datetime.utcnow()


def getLinksFromCMCExchangePage():

    url = 'https://coinmarketcap.com/exchanges/volume/24-hour/'

    _logger.debug('Getting links from {}.'.format(url))

    response = requests.get(url)

    soup = BeautifulSoup(response.content, 'lxml')
    table = soup.find('table')

    exchanges = {}

    for x in table.find_all('a'):
        if '/exchanges' in x['href']:
            link = x['href']
            name = link.split('/')[2]
            exchanges[name] = 'https://coinmarketcap.com{}'.format(link)

    _logger.info('{} Exchanges links found.'.format(len(exchanges)))

    return exchanges


def parseExchangeInfo(exchange_name, page):

    btc_pairs = []
    eth_pairs = []
    usdt_pairs = []
    twitter = ''
    website = ''

    response = requests.get(page)
    soup = BeautifulSoup(response.content, 'html.parser')

    href = soup.find_all('a', target='_blank')

    for x in href:
        line = x.text
        if '/BTC' in line:
            parsed = line.replace('/BTC', '')
            btc_pairs.append(parsed)

        elif '/ETH' in line:
            parsed = line.replace('/ETH', '')
            eth_pairs.append(parsed)

        elif '/USDT' in line:
            parsed = line.replace('/USDT', '')
            usdt_pairs.append(parsed)

        elif '@' in line:
            twitter = line.replace('@', '')

        elif 'https://' in line:
            website = line

    vol = convertDollarsToInt(soup.find('span', {'class': 'text-large2'}).text)
    bvol = soup.find('span', {'class': 'text-gray details-text-medium'}).text
    bvol = convertBTCToFloat(bvol)

    results = {'name': exchange_name,
               'twitter': twitter,
               'website': website,
               'volume_usd': vol,
               'volume_btc': bvol,
               'btc_pairs': ','.join(btc_pairs),
               'eth_pairs': ','.join(eth_pairs),
               'usdt_pairs': ','.join(usdt_pairs),
               'last_updated': getCurrentDateTime()}
    return results


def getAllExchangeInfo():

    exchanges = getLinksFromCMCExchangePage()
    start = time()

    for name, page in exchanges.items():
        print(name, page)
        ExchangeData.addExchange(**parseExchangeInfo(name, page))

    end = time() - start

    log_msg = 'Done parsing {0} exchange pages. It took {1} minutes.'
    log_msg += ' Averaging {2} seconds per page.'

    _logger.info(log_msg.format(
        len(exchanges), end / 60, end / len(exchanges)))


if __name__ == '__main__':
    getAllExchangeInfo()
