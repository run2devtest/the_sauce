from datetime import date
from logger import GetLogger
from urllib.error import HTTPError

import pandas as pd

_logger = GetLogger()


def getVolumeStatistics(cid):
    """Parses Volume Statistics from coinmarketcap historical-data page."""
    try:
        url = 'https://coinmarketcap.com/currencies/{cid}/historical-data/?start={start}&end={end}'
        start = date(2013, 4, 28).isoformat().replace('-', '')
        end = date.today().isoformat().replace('-', '')
        url = url.format(cid=cid, start=start, end=end)

        df = pd.read_html(url)[0]

        if not df['Volume'].isnull().all():  # check if No data

            volume = df['Volume'].replace('-', None).astype(int)

            _mean = int(volume.mean())
            try:
                _std = int(volume.std())
            except ValueError:
                _std = 0
            _min = int(volume.min())
            _max = int(volume.max())
            _bars = int(volume.count())
            _logger.info(
                'Successfully parsed {} volume statistics.'.format(cid))

            return {'mean_volume': _mean, 'std_volume': _std, 'lowest_volume':
                    _min, 'highest_volume': _max, 'bars_recorded': _bars}
        else:
            return {'mean_volume': 0, 'std_volume': 0, 'lowest_volume': 0,
                    'highest_volume': 0, 'bars_recorded': 0}

    except HTTPError:
        _logger.warning('Unable to parse {} volume statistics.'.format(cid))
        return None


if __name__ == '__main__':
    print(getVolumeStatistics('bitcoin'))
