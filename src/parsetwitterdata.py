from datetime import date, datetime
from dbmodels import CoinCapData, CoinCapTwitterData
from dbmodels import ExchangeData, ExchangeTwitterData
from time import time as tm
from twitterparser import getUserStats

from deco import concurrent, synchronized
from logger import GetLogger

from controller import create_Charts

_logger = GetLogger()


def getCurrentDateTime():
    dt = datetime.utcnow()
    return datetime(dt.year, dt.month, dt.day, dt.hour, 30 * (dt.minute // 30))


@concurrent
def getCoinCapTwitterData(time):
    for cid, twitter_handle in CoinCapData.get_all_twitter_names().items():
        data = {**{'cid': cid}, **getUserStats(twitter_handle), **time}
        CoinCapTwitterData.addData(**data)
        _logger.info('{} twitter data recorded...'.format(data['cid']))


@concurrent
def getExchangeTwitterData(time):
    for name, twitter_handle in ExchangeData.get_all_twitter_names().items():
        data = {**{'name': name}, **getUserStats(twitter_handle), **time}
        ExchangeTwitterData.addData(**data)
        _logger.info('{} twitter data recorded...'.format(data['name']))


@synchronized
def parseAllTwitterData():
    time = {'created_date': getCurrentDateTime()}
    getCoinCapTwitterData(time)
    getExchangeTwitterData(time)


if __name__ == '__main__':
    start_time = tm()
    parseAllTwitterData()
    _logger.info(
        'twitter-data-parser took {} minutes.'.format((tm() - start_time) / 60))
    create_Charts(new_data=True)
