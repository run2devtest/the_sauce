from datetime import datetime
from peewee import *
from logger import GetLogger

_logger = GetLogger()

db = PostgresqlDatabase('the_sauce')


class BaseModel(Model):

    class Meta:
        database = db


class CoinCapData(BaseModel):
    cid = CharField(unique=True)  # changed id to cid. id is a python function

    name = CharField(null=True)
    symbol = CharField(null=True)
    rank = IntegerField(null=True)

    twitter = CharField(null=True)

    price_usd = DecimalField(max_digits=19, decimal_places=6, null=True)
    # Satoshi will be multiplied by 100,000,000
    price_btc = BigIntegerField(null=True)

    # make sure data is changed from 24h at the beginning to the end
    volume_24h_usd = BigIntegerField(null=True)
    market_cap_usd = BigIntegerField(null=True)

    available_supply = BigIntegerField(null=True)
    total_supply = BigIntegerField(null=True)
    max_supply = BigIntegerField(null=True)

    mean_volume = BigIntegerField(null=True)
    std_volume = BigIntegerField(null=True)
    lowest_volume = BigIntegerField(null=True)
    highest_volume = BigIntegerField(null=True)
    bars_recorded = BigIntegerField(null=True)

    percent_change_1h = DecimalField(max_digits=8, decimal_places=2, null=True)
    percent_change_24h = DecimalField(
        max_digits=8, decimal_places=2, null=True)
    percent_change_7d = DecimalField(max_digits=8, decimal_places=2, null=True)

    cached = BooleanField()

    created_date = DateTimeField()
    last_updated = DateTimeField(null=True)

    def addCoin(**kwargs):
        try:
            time = {'created_date': getCurrentDateTime()}
            updatedkwargs = {**kwargs, **time}

            CoinCapData.create(**updatedkwargs)
            _logger.info(
                '{} added to coincap-data-table..'.format(kwargs['cid']))

        except IntegrityError:
            db.rollback()
            CoinCapData.updateCoin(**kwargs)
            _logger.info(
                '{} already exists in coincap-data-table..'.format(kwargs['cid']))

    def updateCoin(**kwargs):
        try:
            cid_query = kwargs.pop('cid')
            query = CoinCapData.update(
                **kwargs).where(CoinCapData.cid == cid_query)
            query.execute()
            _logger.info(
                '{} updated in coincap-data-table..'.format(cid_query))

        except CoinCapData.DoesNotExist:
            _logger.info(
                '{} doesnot exist in coincap-data-table..'.format(kwargs['cid']))
            CoinCapData.addCoin(**kwargs)

    def get_all_cids():
        return [row.cid for row in CoinCapData.select()]

    def get_all_twitter_names():
        twitter_names = {
            row.cid: row.twitter for row in CoinCapData.select() if row.twitter is not None}  # like magic!
        return twitter_names

    def get_row_by_cid(name):
        try:
            return CoinCapData.get(CoinCapData.cid == name)

        except CoinCapData.DoesNotExist:
            _logger.info(
                '{} doesnot exist in coincap-data-table...'.format(name))
            return None

    def get_name_from_symbol(symbol):
        try:
            return CoinCapData.get(CoinCapData.symbol == symbol.upper()).name
        except CoinCapData.DoesNotExist:
            _logger.info(
                '{} doesnot exist in coincap-data-table...'.format(name))
            return None

    def get_symbol_from_cid(cid):
        try:
            return CoinCapData.get(CoinCapData.cid == cid).symbol
        except CoinCapData.DoesNotExist:
            _logger.info(
                '{} doesnot exist in coincap-data-table...'.format(name))
            return None

    def get_all_symbols_and_cids():
        symbol_names = {
            row.symbol: row.cid for row in CoinCapData.select() if row.twitter is not None
        }
        return symbol_names

    def get_all_cids_and_symbols():
        symbol_names = {
            row.cid: row.symbol for row in CoinCapData.select() if row.twitter is not None
        }
        return symbol_names


class CoinCapTwitterData(BaseModel):
    cid = ForeignKeyField(CoinCapData, to_field='cid')

    tweets = BigIntegerField(null=True)
    following = BigIntegerField(null=True)
    followers = BigIntegerField(null=True)
    likes = BigIntegerField(null=True)

    created_date = DateTimeField()

    def addData(**kwargs):
        CoinCapTwitterData.create(**kwargs)
        _logger.info(
            '{} added to coincap-twitter-data-table...'.format(kwargs['cid']))

    def getDistictCoinsCids():  # new
        return CoinCapTwitterData.select(CoinCapTwitterData.cid_id).distinct()

    def getDistictDates():
        return CoinCapTwitterData.select(CoinCapTwitterData.created_date).distinct()\
            .order_by(CoinCapTwitterData.created_date.desc())

    def getFollowersByCid(cid, limit=None):
        return CoinCapTwitterData.select(CoinCapTwitterData.cid_id,
                                         CoinCapTwitterData.followers,
                                         CoinCapTwitterData.created_date)\
            .order_by(CoinCapTwitterData.created_date.desc())\
            .where(CoinCapTwitterData.cid_id == cid).limit(limit)


class ExchangeData(BaseModel):
    name = CharField(unique=True)

    twitter = CharField(null=True)
    website = CharField(null=True)

    volume_btc = DecimalField(max_digits=19, decimal_places=6, null=True)
    volume_usd = BigIntegerField(null=True)

    btc_pairs = TextField(null=True)
    eth_pairs = TextField(null=True)
    usdt_pairs = TextField(null=True)
    # make sure data is changed from 24h at the beginning to the end

    created_date = DateTimeField()
    last_updated = DateTimeField(null=True)

    def addExchange(**kwargs):
        try:
            time = {'created_date': getCurrentDateTime()}
            updatedkwargs = {**kwargs, **time}

            ExchangeData.create(**updatedkwargs)
            _logger.info(
                '{} added to exchange-data-table...'.format(kwargs['name']))

        except IntegrityError:
            db.rollback()
            ExchangeData.updateExchange(**kwargs)
            _logger.info(
                '{} already exists in exchange-data-table...'.format(kwargs['name']))

    def updateExchange(**kwargs):
        try:
            name_query = kwargs.pop('name')
            query = ExchangeData.update(
                **kwargs).where(ExchangeData.name == name_query)
            query.execute()
            _logger.info(
                '{} updated in exchange-data-table...'.format(name_query))

        except ExchangeData.DoesNotExist:
            _logger.info(
                '{} doesnot exist in exchange-data-table...'.format(kwargs['name']))
            ExchangeData.addExchange(**kwargs)

    def get_all_exchange_names():
        return [row.name for row in ExchangeData.select()]

    def get_all_twitter_names():
        twitter_names = {
            row.name: row.twitter for row in ExchangeData.select() if len(row.twitter) > 1}  # Fucking Magic
        return twitter_names

    def get_row_by_name(name):
        try:
            return ExchangeData.get(ExchangeData.name == name)

        except ExchangeData.DoesNotExist:
            _logger.info(
                '{} doesnot exist in exchange-data-table...'.format(name))
            return None

    def get_exchanges_vol_1B():
        return [row.name for row in ExchangeData.select(ExchangeData.name).where(
            ExchangeData.volume_usd >= 1000000000)]

    def get_exchanges_vol_500M():
        return [row.name for row in ExchangeData.select(ExchangeData.name).where(
            ExchangeData.volume_usd >= 500000000)]

    def get_exchanges_vol_100M():
        return [row.name for row in ExchangeData.select(ExchangeData.name).where(
            ExchangeData.volume_usd >= 100000000)]

    def get_exchanges_vol_less_100M():
        return [row.name for row in ExchangeData.select(ExchangeData.name).where(
            ExchangeData.volume_usd <= 100000000)]

    def get_exchanges_vol_less_10M():
        return [row.name for row in ExchangeData.select(ExchangeData.name).where(
            ExchangeData.volume_usd <= 10000000)]


class ExchangeTwitterData(BaseModel):
    name = ForeignKeyField(ExchangeData, to_field='name')

    tweets = BigIntegerField(null=True)
    following = BigIntegerField(null=True)
    followers = BigIntegerField(null=True)
    likes = BigIntegerField(null=True)

    created_date = DateTimeField()

    def addData(**kwargs):
        ExchangeTwitterData.create(**kwargs)
        _logger.info(
            '{} added to exchange-twitter-table...'.format(kwargs['name']))


def getCurrentDateTime():
    return datetime.utcnow()


def createTables():
    db.create_tables([CoinCapData, CoinCapTwitterData,
                      ExchangeData, ExchangeTwitterData], True)


def dropTables():
    db.drop_tables([CoinCapData, CoinCapTwitterData,
                    ExchangeData, ExchangeTwitterData], True)


def cleanTables():
    dropTables()
    createTables()
    _logger.info('CoinCapData, ExchangeData Tables Cleaned...')


if __name__ == '__main__':
    pass
    print('hello')
    # print(CoinCapData.get_all_cids())
    # print(CoinCapData.get_row_by_cid('bitcoin'))
    # print(ExchangeData.get_all_twitter_names())
    # print(len(ExchangeData.get_all_twitter_names()))
