import pandas as pd
from collections import Counter

from filtercontroller import getSogFilterCoins, getSogFilterPlusCoins, getBillionFilterCoins, get500MillionFilterCoins, getLowVolFilterShitCoins
from filtercontroller import getLessThan100MillionFilterCoins, getLessThan10MillionFilterCoins
from filtercontroller import getBillionFilterCoinsNoBinace, get500MillionFilterCoinsNoBinace

from dbmodels import CoinCapData


def print_list(dframe, **kwargs):
    min_tweet_size = kwargs['min_tweet_size']
    resample_size = kwargs['resample_size']
    return_size = kwargs['return_size']

    latest = dframe[-1:]
    bools = (latest > min_tweet_size).any(axis=0)
    keep = list(latest.loc[:, bools].columns)
    pct = dframe[keep]
    pct = pct.resample(resample_size).mean().pct_change()

    row = pct[-1:].reset_index().drop('index', axis=1)
    row = row.melt(var_name='coin')
    row = row.sort_values('value').rename(columns={'value': 'percent_change'})
    row['percent_change'] = row['percent_change'] * 100
    return list(row['coin'].tail(return_size))


def create_filtered_plot(filter_):
    df = pd.read_pickle('DATA.pickle')[filter_()]

    dlist = []
    for size in [500, 1000, 2500, 5000, 10000, 25000, 50000]:
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='1H', return_size=5)

        dlist += print_list(df, min_tweet_size=size,
                            resample_size='3H', return_size=5)

        dlist += print_list(df, min_tweet_size=size,
                            resample_size='6H', return_size=5)

        dlist += print_list(df, min_tweet_size=size,
                            resample_size='12H', return_size=5)

        dlist += print_list(df, min_tweet_size=size,
                            resample_size='1D', return_size=5)

        dlist += print_list(df, min_tweet_size=size,
                            resample_size='3D', return_size=5)

        dlist += print_list(df, min_tweet_size=size,
                            resample_size='5D', return_size=5)

        dlist += print_list(df, min_tweet_size=size,
                            resample_size='7D', return_size=5)

    counter = Counter(dlist)
    size = int(len(counter))
    thresh = int(size / 2)
    # dlist = [x[0] for x in counter.most_common()[:thresh]]
    dlist = [x[0] for x in counter.most_common()]

    return dlist


def getCoinList():
    filters = [getSogFilterCoins, getSogFilterPlusCoins, getBillionFilterCoins, get500MillionFilterCoins, getLowVolFilterShitCoins,
               getLessThan100MillionFilterCoins, getLessThan10MillionFilterCoins, getBillionFilterCoinsNoBinace, get500MillionFilterCoinsNoBinace]

    cid_dict = CoinCapData.get_all_cids_and_symbols()
    coins = set()
    for f in filters:
        for coin in create_filtered_plot(f):
            coins.add(cid_dict[coin])

    return list(coins)

if __name__ == '__main__':
    print(getCoinList())
