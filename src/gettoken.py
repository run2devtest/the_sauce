def GetToken():
    """Parses TOKEN file. Strips newlines and
    whitespaces. Returns token string.
    """
    with open('TOKEN', 'r') as TOKEN:
        return TOKEN.read().strip('\n').strip(' ')


def GetToken2():
    """Parses TOKEN file. Strips newlines and
    whitespaces. Returns token string.
    """
    with open('TOKEN2', 'r') as TOKEN:
        return TOKEN.read().strip('\n').strip(' ')

if __name__ == '__main__':
    print(GetToken())
