class Ichimoku():

    def __init__(self, df):
        self.data = df

        self.conversion_periods = 10
        self.base_periods = 30
        self.leading_span_periods = 60
        self.displacement = 30

        self.calculate_tenkans()
        self.calculate_kinjuns()
        self.calculate_leading_spanA()
        self.calculate_leading_spanB()
        self.calculate_candle_colors()

    def get_ichimoku_data(self):
        return self.data

    def calculate_donchain(self, periods):
        return (self.data['low'].rolling(periods).min() + self.data['high'].rolling(periods).max()) / 2

    # Tenkan Calculations

    def calculate_tenkans(self):
        self.data['Tenken'] = self.calculate_donchain(
            self.conversion_periods)
        self.data['Tenken_DBL'] = self.calculate_donchain(
            self.conversion_periods * 2)
        self.data['Tenken_HALF'] = self.calculate_donchain(
            self.conversion_periods // 2)

    # Kinjun Calculationis

    def calculate_kinjuns(self):
        self.data['Kinjun'] = self.calculate_donchain(self.base_periods)
        self.data['Kinjun_DBL'] = self.calculate_donchain(
            self.base_periods * 2)
        self.data['Kinjun_HALF'] = self.calculate_donchain(
            self.base_periods // 2)

    def calculate_leading_spanA(self):
        self.data['Leading_Span_A'] = self.data[
            ['Tenken', 'Kinjun']].mean(axis=1).shift(self.displacement)
        self.data['Leading_Span_A_DBL'] = self.data[
            ['Tenken_DBL', 'Kinjun_DBL']].mean(axis=1).shift(self.displacement)
        self.data['Leading_Span_A_HALF'] = self.data[
            ['Tenken_HALF', 'Kinjun_HALF']].mean(axis=1).shift(self.displacement)

    def calculate_leading_spanB(self):
        self.data['Leading_Span_B'] = self.calculate_donchain(
            self.leading_span_periods).shift(self.displacement)
        self.data['Leading_Span_B_DBL'] = self.calculate_donchain(
            self.leading_span_periods * 2).shift(self.displacement)
        self.data['Leading_Span_B_HALF'] = self.calculate_donchain(
            self.leading_span_periods // 2).shift(self.displacement)

    def create_candles(row):
        close = row['close']

        lspanA = row['Leading_Span_A']
        lspanA_DBL = row['Leading_Span_A_DBL']
        lspanA_HALF = row['Leading_Span_A_HALF']

        lspanB = row['Leading_Span_B']
        lspanB_DBL = row['Leading_Span_B_DBL']
        lspanB_HALF = row['Leading_Span_B_HALF']

        price_below_cloud = close < min(
            [lspanA, lspanA_DBL, lspanA_HALF, lspanB, lspanB_DBL, lspanB_HALF])
        price_above_cloud = close > max(
            [lspanA, lspanA_DBL, lspanA_HALF, lspanB, lspanB_DBL, lspanB_HALF])

        if price_below_cloud:
            return 'red'
        elif price_above_cloud:
            return 'green'
        else:
            return 'green'

    def calculate_candle_colors(self):
        self.data['candle_color'] = self.data.apply(
            Ichimoku.create_candles, axis=1)

    def get_price_cloud_relation(self):
        row = self.data.tail(1)
        close = float(row['close'])

        lspanA = float(row['Leading_Span_A'])
        lspanA_DBL = float(row['Leading_Span_A_DBL'])
        lspanA_HALF = float(row['Leading_Span_A_HALF'])

        lspanB = float(row['Leading_Span_B'])
        lspanB_DBL = float(row['Leading_Span_B_DBL'])
        lspanB_HALF = float(row['Leading_Span_B_HALF'])

        cloud_bottom = min([lspanA, lspanA_DBL, lspanA_HALF,
                            lspanB, lspanB_DBL, lspanB_HALF])
        cloud_top = max([lspanA, lspanA_DBL, lspanA_HALF,
                         lspanB, lspanB_DBL, lspanB_HALF])

        price_below_cloud = close < cloud_top and close < cloud_bottom
        price_above_cloud = close > cloud_top and close > cloud_bottom
        price_inside_cloud = close <= cloud_top and close >= cloud_bottom

        if price_below_cloud:
            return 'Below'
        elif price_above_cloud:
            return 'Above'
        elif price_inside_cloud:
            return 'Inside'
        else:
            return 'Unknown'
